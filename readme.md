# Wallet


We have a wallet of stock values (different currencies).
We want to known the total value in a specific currency.
For rate exchange, you can use http://api.fixer.io/, google or yahoo.


## Notes

1. Each currency has it's own precision (EUR is 2, XBT is 8), value of given currency should then rounded accordingly
2. This kata has two differents parts :
  - wallet and stock, currency (training on calisthenic objects and SOLID)
  - rate exchange (training on testing external API)


## Extension

1. Be able to get the best rate from different rate api.
2. Be able to audit results



## Examples

### Java code

The java code contains some mistakes :
- some concepts are not well named (Price)
- real rates provider are not implemented


### Python code

This code hase same naming problem too (Stock)
