package katas.wallet;

import com.google.common.base.Objects;

import java.math.BigDecimal;

public final class Price {
    private final Currency currency;
    private final BigDecimal amount;

    public static Price price(final Currency currency, final String amount) {
        return price(currency, new BigDecimal(amount));
    }

    public static Price price(final Currency currency, final double amount) {
        return price(currency, BigDecimal.valueOf(amount));
    }

    public static Price price(final Currency currency, final long amount) {
        return price(currency, BigDecimal.valueOf(amount));
    }

    public static Price price(final Currency currency, final BigDecimal amount) {
        return new Price(currency, amount);
    }

    public static Price price(final String price) {
        final String[] tokens = price.trim().split(" ");
        return price(Currency.valueOf(tokens[0]), tokens[1]);
    }

    private Price(Currency currency, BigDecimal amount) {
        this.currency = currency;
        this.amount = amount;
    }

    public Price add(final Price another) {
        return price(currency, amount.add(another.amount));
    }

    public Price changeTo(Currency currency, RateProvider rateProvider) {
        if (this.currency == currency) {
            return this;
        }
        return price(currency, amount.multiply(rateProvider.rateFor(this.currency, currency).value()));
    }

    @Override
    public String toString() {
        return currency + " " + currency.render(amount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Price price = (Price) o;
        return Objects.equal(currency, price.currency) &&
                Objects.equal(currency.render(amount), currency.render(price.amount));
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(currency, amount);
    }
}
