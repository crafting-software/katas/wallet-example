package katas.wallet;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.collect.Maps.newHashMap;

public final class FixedRateProvider implements RateProvider {
    private static final Pattern RATE_DEFINITION = Pattern.compile("(\\p{Upper}+)/(\\p{Upper}+)\\s+(.+)");
    private final Map<String, Rate> rates = newHashMap();

    public static RateProvider rates(final String... rates) {
        FixedRateProvider provider = fixed();
        for (String definition : rates) {
            Matcher result = RATE_DEFINITION.matcher(definition);
            if (result.matches()) {
                provider = provider.rate(Currency.valueOf(result.group(1)), Currency.valueOf(result.group(2)), Double.parseDouble(result.group(3)));
            }
        }
        return provider;
    }

    public static FixedRateProvider fixed() {
        return new FixedRateProvider();
    }

    public FixedRateProvider rate(Currency from, Currency to, double rate) {
        rates.put(key(from, to), Rate.of(rate));
        return this;
    }

    public Rate rateFor(final Currency from, final Currency to) {
        return rates.get(key(from, to));
    }

    private static String key(Currency from, Currency to) {
        return from + "/" + to;
    }
}
